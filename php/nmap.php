<?php $hosts = shell_exec("nmap -sn 192.168.1.0/24 | awk -F \" |/\" '/report/ {print $5,$6}'");
 // $addresses = shell_exec("nmap -sn 192.168.1.0/24 | awk -F \" |/\" '/report/ {print $6}'");
 // $addresses = str_replace("(", "", $addresses);
 // $addresses = str_replace(")", "", $addresses);
?>

<?php if ($hosts): ?>
    <div class="row">
        <div class="col-lg-12 text-center">
            <h2>Hôtes connectés : </h2>
            <hr class="star-primary">
        </div>
        <div class="col-lg-12 col-lg-offset-4">
                
            <?php $hosts = explode("\n", $hosts) ?>
            <?php foreach ($hosts as $h): ?>
                <p><?= $h ?></p>
            <?php endforeach ?>

        </div>
    </div>
<?php endif ?>