$(window).ready(function() {  

	var already = true;

	$('.project-tp').css('opacity', '0');
	$('#popup').hide();
    var scroll = $(window).scrollTop();

    if (scroll >= 0 && scroll < 250) {
        $('.project-tp').addClass('fadeInRight').css('opacity','1');
    }
    else if (scroll >= 250 && scroll < 1150) {
        networkPopOut();
        $('.project-tp').removeClass('fadeInRight');

        if (already) {

			$('.progressChild').progBar({
				finish : 100,
				start : 0,
				speed : 5,
				title : 'fini'
			});
			already = false;
        }

    }


	$(window).scroll(function() {   
	    var scroll = $(window).scrollTop();
        if (scroll >= 0 && scroll < 250) {
	        $('.project-tp').addClass('fadeInRight').css('opacity','1');
    	}
	    else if (scroll >= 250 && scroll < 1150) {
	    	networkPopOut();

	        if (already) {

				$('.progressChild').progBar({
					finish : 100,
					start : 0,
					speed : 5,
					title : 'fini'
				});
				already = false;
	        }
	    }

	});


	$('.popout').hover(function() {

		var top = $(this).offset().top -70;
		var left = $(this).offset().left;
		var addrIp = $(this).data('ip') || "";
		var desc = $(this).attr('alt') + "<br>" + addrIp;
		console.log(desc);
		$('#popup .content').html(desc);
		$('#popup').fadeIn().css({
			top: top,
			left: left
		});
	});

	$('#popup .quit').on('click', function(e) {
		$('#popup').hide();
	});

	$.ajax({
		url: 'php/nmap.php',
		type: 'GET',
	})
	.done(function(resultat) {
		$('.nmap').append(resultat);
	})
	.always(function() {
		$('.loading').hide();
	});
	
});