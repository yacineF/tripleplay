<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Triple Play</title>

    <!-- Bootstrap Core CSS - Uses Bootswatch Flatly Theme: http://bootswatch.com/flatly/ -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#page-top">Triple Play</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#portfolio">Réalisation</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Etudiants</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 project-tp">
                    <img class="img-responsive" width="220px" src="img/cloud-g.png" alt="">
                    <div class="intro-text">
                        <span class="name">Projet Triple Play</span>
                        <hr class="star-light">
                        <span class="skills">Qualité de service - Voix, Vidéo, Donnée</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Réalisation</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
            <div id="popup">
              <div class="content">
              </div>
              <div class="quit">X</div>
            </div>
                
                <div class="col-lg-12 network-icons text-center">
                
                    <img src="img/www.png" class="" alt="Serveur Web" data-ip="10.0.2.10/24">
                    <div class="progress hidden-sm hidden-xs"> 

                    </div>
                    <img src="img/switch.png" class="" alt="Switch" data-ip="10.0.0.0/24">
                    <div class="progress hidden-sm hidden-xs">
 
                    </div>
                    <img data-ip="" src="img/bas.png" class="" alt="BAS">
                    
                    <div class="progress hidden-sm hidden-xs">
                    </div>
                    <img data-ip="" src="img/dslam.png" class="" alt="DSLAM">
                    

                    <div class="progress hidden-sm hidden-xs">
                    </div>
                    <img data-ip="192.168.1.0/24" src="img/settopbox.png" class="" alt="Box">
                    
                    <div class="progress hidden-sm hidden-xs"> 
                    </div>
                    <img data-ip="192.168.1.10/24" src="img/pc.png" class="" alt="PC">
                </div>

                <div class="vertical progress hidden-sm hidden-xs progress-left">  
                </div>

                <div class="vertical progress hidden-sm hidden-xs progress-right">  
                </div>

                <div class="col-lg-12 network-icons decaled">
                    <img data-ip="10.0.3.10/24" src="img/ctrs.png" class="" alt="Serveur vidéo">

                    <img data-ip="192.168.1.20/24" src="img/tele.png" class="right-decaled" alt="Télévision">
                    <div class="clear"></div>
                <div class="vertical progress hidden-sm hidden-xs progress-left-second">
                </div>  

                <div class="vertical progress hidden-sm hidden-xs progress-right-second">
                </div>
                </div>

                <div class="col-lg-12 network-icons decaled">
                    <img data-ip="10.0.4.10/24" src="img/xt30.png" class="" alt="Serveur téléphonique">
                    <img data-ip="192.168.1.30/24" src="img/phone.png" class="right-decaled" alt="Téléphone">
                </div>

            </div>

            <div class="row margin-t">
                <div class="text-center">
                    <img src="img/ajax-loader.gif" class="loading">
                </div>

                <div class="nmap">

                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Etudiants :</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row student">
                <div class="col-lg-4 col-lg-offset-3">
                    <p>Yacine Floret</p>
                    <p>Quentin Jacquiet</p>
                    <p>Kalamas Philippe</p>
                    <p>Kevin Prince</p>
                </div>
                <div class="col-lg-4">
                    <p>Brousse Victorien</p>
                    <p>Devernois Axel</p>
                    <p>Lhermite Julien</p>
                    <p>Osmani Damien</p>
                </div>

            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Etablissement </h3>
                        <p>IUT Dijon Auxerre<br>
                        Route des plaines de l'yonne<br>
                        89000 Auxerre<br>
                        France</p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Matériel</h3>
                        <p>
                            Switch, Routeur, BAS, Serveur, Téléphone IP, DSLAM, Box ADSL, PC
                        </p>
                    </div>
                    <div class="footer-col col-md-4 no-center">
                        <h3>Enseignants</h3>
                        <p>Marcel Séverine <br>
                        Roy Michaël <br>
                        Mazerolles Alban <br>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll visible-xs visible-sm">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>



    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/freelancer.js"></script>
    <script src="js/function.js"></script>
    <script src="js/jQuery.progBar.js"></script>
    <script src="js/app.js"></script>

</body>

</html>
