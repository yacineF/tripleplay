\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Objectif}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Topologie}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Configuration IP et OSPF}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Routeur de bordure}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Backbone router}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Configuration poste client}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Diagnostic}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Configuration de la QoS}{8}{section.3}
\contentsline {subsection}{\numberline {3.1}QoS marking}{8}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}QoS queuing}{9}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Policing}{10}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Shaping}{11}{subsubsection.3.2.2}
