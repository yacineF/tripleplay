 \NeedsTeXFormat{LaTeX2e}
\ProvidesClass{CR} 
% le nom de ce fichier est ma_classe.cls
\LoadClass[a4paper,french,twoside,12pt]{article}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage[francais]{babel}
\RequirePackage{soul}
\RequirePackage{hyperref}
\RequirePackage{enumerate}
\RequirePackage{multirow}
\RequirePackage{textcomp}
\RequirePackage{titlesec}
\RequirePackage{amssymb}
\RequirePackage{fancyvrb}
\RequirePackage{fancyhdr}
\RequirePackage{listings}
\RequirePackage{graphicx}
\RequirePackage[top = 1.5cm,bottom = 2cm,left = 1cm,right = 1cm]{geometry}
\pagestyle{fancy}
\RequirePackage{color}
\RequirePackage{setspace}

\setlength{\marginparwidth}{2cm}
\titleformat*{\section}{\Large\fontfamily{lmss}\bfseries}
\titleformat*{\subsection}{\large\fontfamily{lmss}\bfseries}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\DefineVerbatimEnvironment{terminal}{Verbatim}{frame=single,framesep=3mm}

\renewcommand{\headrulewidth}{0pt}
\fancyhead[R]{}
\fancyhead[L]{}
\renewcommand{\footrulewidth}{0.5pt}
\fancyfoot[R]{Triple Play}
\fancyfoot[L]{\includegraphics[scale=0.3]{iut-dijon-logo.png}}
\fontfamily{lmss}\selectfont

\lstset{literate=
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\EUR}}1 {£}{{\pounds}}1
}
